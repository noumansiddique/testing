<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "divs".
 *
 * @property integer $div_id
 * @property string $div_name
 * @property integer $div_weight
 * @property integer $pages_page_id
 * @property integer $landingpages_id
 * @property string $div_status
 *
 * @property Landingpages $landingpages
 * @property Pages $pagesPage
 */
class Divs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'divs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['div_name', 'div_weight', 'pages_page_id', 'div_status'], 'required'],
            [['div_weight', 'pages_page_id', 'landingpages_id'], 'integer'],
            [['div_status'], 'string'],
            [['div_name'], 'string', 'max' => 100],
            [['landingpages_id'], 'exist', 'skipOnError' => true, 'targetClass' => Landingpages::className(), 'targetAttribute' => ['landingpages_id' => 'landingpages_id']],
            [['pages_page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pages::className(), 'targetAttribute' => ['pages_page_id' => 'page_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'div_id' => 'Div ID',
            'div_name' => 'Div Name',
            'div_weight' => 'Div Weight',
            'pages_page_id' => 'Pages Page ID',
            'landingpages_id' => 'Landingpages ID',
            'div_status' => 'Div Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLandingpages()
    {
        return $this->hasOne(Landingpages::className(), ['landingpages_id' => 'landingpages_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagesPage()
    {
        return $this->hasOne(Pages::className(), ['page_id' => 'pages_page_id']);
    }
}
