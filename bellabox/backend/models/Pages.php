<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property integer $page_id
 * @property string $page_name
 * @property string $page_header
 * @property string $page_html
 * @property string $page_status
 *
 * @property Divs[] $divs
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_name', 'page_header', 'page_status'], 'required'],
            [['page_html', 'page_status'], 'string'],
            [['page_name', 'page_header'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_id' => 'Page ID',
            'page_name' => 'Page Name',
            'page_header' => 'Page Header',
            'page_html' => 'Page Html',
            'page_status' => 'Page Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivs()
    {
        return $this->hasMany(Divs::className(), ['pages_page_id' => 'page_id']);
    }
}
