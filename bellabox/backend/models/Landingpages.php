<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "landingpages".
 *
 * @property integer $landingpages_id
 * @property string $landingpages_name
 *
 * @property Divs[] $divs
 */
class Landingpages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'landingpages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['landingpages_name'], 'required'],
            [['landingpages_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'landingpages_id' => 'Landingpages ID',
            'landingpages_name' => 'Landingpages Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivs()
    {
        return $this->hasMany(Divs::className(), ['landingpages_id' => 'landingpages_id']);
    }
}
