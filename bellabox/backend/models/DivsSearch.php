<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Divs;

/**
 * DivsSearch represents the model behind the search form about `backend\models\Divs`.
 */
class DivsSearch extends Divs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['div_id', 'div_weight', 'pages_page_id', 'landingpages_id'], 'integer'],
            [['div_name', 'div_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Divs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'div_id' => $this->div_id,
            'div_weight' => $this->div_weight,
            'pages_page_id' => $this->pages_page_id,
            'landingpages_id' => $this->landingpages_id,
        ]);

        $query->andFilterWhere(['like', 'div_name', $this->div_name])
            ->andFilterWhere(['like', 'div_status', $this->div_status]);

        return $dataProvider;
    }
}
