<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\Pages;

/* @var $this yii\web\View */
/* @var $model backend\models\Divs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="divs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'div_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'div_weight')->textInput() ?>

    <?= $form->field($model, 'pages_page_id')->dropDownList(
        ArrayHelper::map(Pages::find()->all(), 'page_id', 'page_name'),
             ['prompt'=>'Select Page',              
            ]); ?> 

    <?= $form->field($model, 'landingpages_id')->textInput() ?>

    <?= $form->field($model, 'div_status')->dropDownList([ 'inactive' => 'Inactive', 'active' => 'Active', ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
