<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DivsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="divs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'div_id') ?>

    <?= $form->field($model, 'div_name') ?>

    <?= $form->field($model, 'div_weight') ?>

    <?= $form->field($model, 'pages_page_id') ?>

    <?= $form->field($model, 'landingpages_id') ?>

    <?php // echo $form->field($model, 'div_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
