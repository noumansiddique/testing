<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Divs */

$this->title = 'Update Divs: ' . $model->div_id;
$this->params['breadcrumbs'][] = ['label' => 'Divs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->div_id, 'url' => ['view', 'id' => $model->div_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="divs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
