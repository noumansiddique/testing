<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Divs */

$this->title = $model->div_id;
$this->params['breadcrumbs'][] = ['label' => 'Divs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="divs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->div_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->div_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'div_id',
            'div_name',
            'div_weight',
            'pages_page_id',
            'landingpages_id',
            'div_status',
        ],
    ]) ?>

</div>
