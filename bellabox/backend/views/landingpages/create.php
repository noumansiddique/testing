<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Landingpages */

$this->title = 'Create Landingpages';
$this->params['breadcrumbs'][] = ['label' => 'Landingpages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landingpages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsDiv' => $modelsDiv,
    ]) ?>

</div>
